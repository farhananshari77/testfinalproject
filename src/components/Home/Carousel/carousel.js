import React from "react";
import "./carousel.css";
import OwlCarousel from "react-owl-carousel";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";
import banner from "../../../assets/images/banner.png";

export default function carousel() {
  return (
    // className "owl-theme" is optional
    <OwlCarousel
      className="owl-theme"
      items={2}
      margin={20}
      loop={true}
      center={true}
      nav={true}
      autoplay={true}
      responsiveClass={true}
      responsive={{
        0: {
          items: 1,
          nav: true
        },
        576: {
          items: 1,
          nav: true
        },
        768: {
          items: 2,
          nav: true
        }
      }}>
      <div className="item">
        <img src={banner} alt="banner" />
      </div>
      <div className="item">
        <img src={banner} alt="banner" />
      </div>
      <div className="item">
        <img src={banner} alt="banner" />
      </div>
    </OwlCarousel>
  );
}
