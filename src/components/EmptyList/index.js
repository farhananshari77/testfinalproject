import React from "react";
import "./styles.css";

// eslint-disable-next-line react/prop-types
const EmptyList = ({ type }) => {
  return (
    <div className="col-lg-12 w-100 d-flex flex-column justify-content-center align-items-center empty-list">
      <img src="images/undraw_selection_re_ycpo 1.png" alt="empty-list" />
      {type === "transaksi" && (
        <p className="mt-5 text-empty">
          Belum ada transaksi di akunmu, lakukan transaksi yuk biar ini keisi
        </p>
      )}
      {type === "home" && <p className="mt-5 text-empty">Belum ada barang untuk kategori ini</p>}
      {type === "wishlist" && (
        <p className="mt-5 text-empty">Belum ada barang yang tersimpan di wishlistmu</p>
      )}
      {type === "diminati" && (
        <p className="mt-5 text-empty">
          Belum ada produkmu yang diminati nih, sabar ya rejeki nggak kemana kok
        </p>
      )}
      {type === "terjual" && (
        <p className="mt-5 text-empty">
          Belum ada produkmu yang terjual nih, sabar ya rejeki nggak kemana kok
        </p>
      )}
    </div>
  );
};

export default EmptyList;
