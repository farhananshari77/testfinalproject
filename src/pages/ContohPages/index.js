import React from "react";
import ContohComponent from "../../components/ContohComponent";

const ContohPages = () => {
  return (
    <div>
      <h1>Contoh Pages</h1>
      <ContohComponent />
    </div>
  );
};

export default ContohPages;
