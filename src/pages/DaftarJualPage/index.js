import React, { useEffect, useState } from "react";
import "./styles.css";
import { Link } from "react-router-dom";
import ProductCard from "../../components/ProductCard";
import EmptyList from "../../components/EmptyList";
import PopUpNotifikasi from "../../components/PopUpNotifikasi";
import ProductCardLebar from "../../components/ProductCardLebar";
import axios from "axios";
import jwt_decode from "jwt-decode";

const DaftarJualPage = () => {
  const bearerToken = localStorage.getItem("token");
  const isLogin = bearerToken !== null;
  const getUserData = jwt_decode(bearerToken);

  const dateNow = new Date();
  const dateNowConvert = dateNow.getTime().toString().slice(0, 10);

  if (isLogin === false || getUserData.exp < dateNowConvert) {
    window.location.href = "/login";
  }

  const urlSemuaBarang = "https://secondhand-api-fsw14-kel3.herokuapp.com/api/barang/listku";
  const urlBarangkuDitawar = "https://secondhand-api-fsw14-kel3.herokuapp.com/api/tawaran/barangku";
  const urlBarangkuTerjual =
    "https://secondhand-api-fsw14-kel3.herokuapp.com/api/tawaran/status-barang";

  const [semuaBarang, setSemuaBarang] = useState([]);
  const [barangkuDitawar, setBarangkuDitawar] = useState([]);
  const [barangkuTerjual, setBarangkuTerjual] = useState([]);

  const getDataSemuaBarang = async () => {
    await axios
      .get(urlSemuaBarang, {
        headers: {
          Authorization: `Bearer ${bearerToken}`
        }
      })
      .then((res) => {
        setSemuaBarang(res.data);
      });
  };

  const getBarangkuDitawar = async () => {
    await axios
      .get(urlBarangkuDitawar, {
        headers: {
          Authorization: `Bearer ${bearerToken}`
        }
      })
      .then((res) => {
        setBarangkuDitawar(res.data);
      });
  };

  const getBarangkuTerjual = async () => {
    await axios
      .get(urlBarangkuTerjual, {
        headers: {
          Authorization: `Bearer ${bearerToken}`
        }
      })
      .then((res) => {
        setBarangkuTerjual(res.data);
      });
  };

  useEffect(() => {
    getDataSemuaBarang();
    getBarangkuDitawar();
    getBarangkuTerjual();
  }, []);

  const semuaBarangs = semuaBarang.data;
  const barangkuDitawars = barangkuDitawar.data;
  const barangkuTerjuals = barangkuTerjual.data ? barangkuTerjual.data : [];

  const [getUser, setGetUser] = useState([]);

  useEffect(() => {
    const getPost = async () => {
      await axios
        .get("https://secondhand-api-fsw14-kel3.herokuapp.com/api/users/profile", {
          headers: {
            Authorization: `Bearer ${bearerToken}`
          }
        })
        .then((res) => {
          setGetUser(res.data);
        });
    };
    getPost();
  }, []);

  const dataFotoUser = getUser.data ? getUser.data.foto_user : "loading";

  return (
    <div className="container-fluid daftar-jual" style={{ marginTop: "100px" }}>
      <PopUpNotifikasi />
      <div className="row justify-content-center">
        <div className="col-lg-9">
          <h3 className="title">Daftar Jual Saya</h3>
          <div className="daftar-jual-seller mx-auto d-flex justify-content-between align-items-center">
            <img src={dataFotoUser} alt="avatar" />
            <div className="flex-fill">
              <h5 className="title">{getUserData.nama}</h5>
              <p className="text">Kota</p>
            </div>
            <Link
              to="/inputProfile"
              className="button d-flex justify-content-center align-items-center">
              Edit
            </Link>
          </div>
          <div className="row">
            <div className="d-flex align-items-start">
              <div className="col-lg-3">
                <div className="daftar-jual-kategori">
                  <h5 className="title">Kategori</h5>
                  <div
                    className="nav flex-column nav-pills"
                    id="v-pills-tab"
                    role="tablist"
                    aria-orientation="vertical">
                    <button
                      className="nav-link active"
                      id="v-pills-semua-tab"
                      data-bs-toggle="pill"
                      data-bs-target="#v-pills-semua"
                      type="button"
                      role="tab"
                      aria-controls="v-pills-semua"
                      aria-selected="true">
                      <div className="d-flex justify-content-between">
                        <img src="icons/box.svg" alt="box-icons" />
                        <p className="flex-fill">Semua Produk</p>
                        <img src="icons/chevron-right-active.svg" alt="chevron-right-icon" />
                      </div>
                    </button>
                    <button
                      className="nav-link"
                      id="v-pills-diminati-tab"
                      data-bs-toggle="pill"
                      data-bs-target="#v-pills-diminati"
                      type="button"
                      role="tab"
                      aria-controls="v-pills-diminati"
                      aria-selected="false">
                      <div className="d-flex justify-content-between">
                        <img src="icons/heart.svg" alt="heart-icons" />
                        <p className="flex-fill">Diminati</p>
                        <img src="icons/chevron-right.svg" alt="chevron-right-icon" />
                      </div>
                    </button>
                    <button
                      className="nav-link"
                      id="v-pills-terjual-tab"
                      data-bs-toggle="pill"
                      data-bs-target="#v-pills-terjual"
                      type="button"
                      role="tab"
                      aria-controls="v-pills-terjual"
                      aria-selected="false">
                      <div className="d-flex justify-content-between">
                        <img src="icons/dollar-sign.svg" alt="dollar-sign-icons" />
                        <p className="flex-fill">Terjual</p>
                        <img src="icons/chevron-right.svg" alt="chevron-right-icon" />
                      </div>
                    </button>
                  </div>
                </div>
              </div>
              <div className="daftar-jual-kategori-mobile">
                <ul className="nav nav-pills mb-3" id="pills-tab" role="tablist">
                  <li className="nav-item" role="presentation">
                    <button
                      className="nav-link active"
                      id="v-pills-semua-tab"
                      data-bs-toggle="pill"
                      data-bs-target="#v-pills-semua"
                      type="button"
                      role="tab"
                      aria-controls="v-pills-semua"
                      aria-selected="true">
                      <div className="d-flex justify-content-between ">
                        <img src="icons/box.svg" alt="box-icons" />
                        <p className="flex-fill">Semua Produk</p>
                      </div>
                    </button>
                  </li>
                  <li className="nav-item" role="presentation">
                    <button
                      className="nav-link"
                      id="v-pills-diminati-tab"
                      data-bs-toggle="pill"
                      data-bs-target="#v-pills-diminati"
                      type="button"
                      role="tab"
                      aria-controls="v-pills-diminati"
                      aria-selected="false">
                      <div className="d-flex justify-content-between ">
                        <img src="icons/heart.svg" alt="heart-icons" />
                        <p className="flex-fill">Diminati</p>
                      </div>
                    </button>
                  </li>
                  <li className="nav-item" role="presentation">
                    <button
                      className="nav-link"
                      id="v-pills-terjual-tab"
                      data-bs-toggle="pill"
                      data-bs-target="#v-pills-terjual"
                      type="button"
                      role="tab"
                      aria-controls="v-pills-terjual"
                      aria-selected="false">
                      <div className="d-flex justify-content-between ">
                        <img src="icons/dollar-sign.svg" alt="dollar-sign-icons" />
                        <p className="flex-fill">Terjual</p>
                      </div>
                    </button>
                  </li>
                </ul>
              </div>
              <div className="tab-content" id="v-pills-tabContent">
                <div
                  className="tab-pane fade show active"
                  id="v-pills-semua"
                  role="tabpanel"
                  aria-labelledby="v-pills-semua-tab">
                  <div className="col-lg-12 mx-auto row daftar-jual-cards">
                    <Link
                      to="/inputProduk"
                      className="col-lg-4 col-sm-12 mb-4 daftar-jual-card tambah-daftar-jual-card d-flex flex-column justify-content-center align-items-center">
                      <img src="icons/plus.svg" alt="plus-icon" />
                      <p>Tambah Produk</p>
                    </Link>
                    {semuaBarangs == undefined
                      ? ""
                      : semuaBarangs.map((item) => {
                          const image = JSON.parse(item.foto_produk);
                          return (
                            <div key={item.id} className="col-lg-4 col-sm-12 mb-4 daftar-jual-card">
                              <ProductCard
                                img={image[0]}
                                name={item.nama_produk}
                                category={item.kategori}
                                price={item.harga_produk}
                                href={`/detailProduct?id=${item.id}`}
                              />
                            </div>
                          );
                        })}
                  </div>
                </div>
                <div
                  className="tab-pane fade daftar-jual-tab-pane"
                  id="v-pills-diminati"
                  role="tabpanel"
                  aria-labelledby="v-pills-diminati-tab">
                  <div className="col-lg-10 mx-auto row daftar-jual-cards">
                    {barangkuDitawars == undefined ? (
                      <EmptyList type="diminati" />
                    ) : barangkuDitawars.length === 0 ? (
                      <EmptyList type="diminati" />
                    ) : (
                      barangkuDitawars.map((item) => {
                        const image = JSON.parse(item.Barang.foto_produk);
                        return (
                          <ProductCardLebar
                            key={item.id}
                            harga={item.Barang.harga_produk}
                            nama={item.Barang.nama_produk}
                            hargaPenawaran={item.harga_tawaran == null ? "0" : item.harga_tawaran}
                            tanggalPenawaran={item.updatedAt}
                            type="Penawaran"
                            href={`/detaiPenawaran?id=${item.id}`}
                            image={image[0]}
                          />
                        );
                      })
                    )}
                  </div>
                </div>
                <div
                  className="tab-pane fade daftar-jual-tab-pane"
                  id="v-pills-terjual"
                  role="tabpanel"
                  aria-labelledby="v-pills-terjual-tab">
                  <div className="col-lg-10 mx-auto row daftar-jual-cards">
                    {barangkuTerjuals == [] ? (
                      <EmptyList type="terjual" />
                    ) : barangkuTerjuals.length === 0 ? (
                      <EmptyList type="terjual" />
                    ) : (
                      barangkuTerjuals.map((item) => {
                        const image = JSON.parse(item.Barang.foto_produk);
                        return (
                          <ProductCardLebar
                            key={item.Barang.id}
                            harga={item.Barang.harga_produk}
                            nama={item.nama_produk}
                            hargaPenawaran={item.harga_tawaran == null ? "0" : item.harga_tawaran}
                            tanggalPenawaran={item.updatedAt}
                            type="Penawaran"
                            href={`/detailProduct?id=${item.Barang.id}`}
                            image={image[0]}
                          />
                        );
                      })
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DaftarJualPage;
