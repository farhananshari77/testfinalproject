/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from "react";
import "./styles.css";
import axios from "axios";
import CurrencyFormatter from "../../modules/CurrencyFormatter";
import jwt_decode from "jwt-decode";
import { Link } from "react-router-dom";

const DetailProductPage = () => {
  const [sudahTawar, setSudahTawar] = useState(false);
  const [hargaTawar, setHargaTawar] = useState("");
  const [alert, setAlert] = useState(false);
  const [alertWishlist, setAlertWishlist] = useState(false);
  const [dataPenawaranLocalStorage, setDataPenawaranLocalStorage] = useState([]);
  const [wishlist, setWishlist] = useState([]);
  const [sudahWishlist, setSudahWishList] = useState(false);
  const [dataProduct, setDataProduct] = useState([]);

  const queryParams = new URLSearchParams(window.location.search);
  const productSelect = queryParams.get("id");

  useEffect(() => {
    const items = JSON.parse(localStorage.getItem("dataPenawaran"));
    if (items) {
      setDataPenawaranLocalStorage(items);
    }

    const itemsWishlist = JSON.parse(localStorage.getItem("wishlist"));
    if (itemsWishlist) {
      setWishlist(itemsWishlist);
    }
  }, []);

  setTimeout(() => {
    setAlert(false);
  }, 10000);

  const url = `https://secondhand-api-fsw14-kel3.herokuapp.com/api/barang/${productSelect}`;
  const getPost = async () => {
    await axios.get(url).then((res) => {
      setDataProduct(res.data);
    });
  };

  useEffect(() => {
    getPost();
  }, []);

  const bearerToken = localStorage.getItem("token");
  const isLogin = bearerToken !== null;

  const getUserData = jwt_decode(bearerToken);

  const dateNow = new Date();
  const dateNowConvert = dateNow.getTime().toString().slice(0, 10);

  const dataFotoProduct = dataProduct.data ? JSON.parse(dataProduct.data.foto_produk) : "loading";

  const urlTawar = `https://secondhand-api-fsw14-kel3.herokuapp.com/api/tawaran/upload/${productSelect}`;

  const tawarBarang = async () => {
    const data = {
      harga_tawaran: hargaTawar
    };
    let harga = await fetch(urlTawar, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${bearerToken}`
      },
      body: JSON.stringify(data)
    })
      .then((res) => res.json())
      .then((data) => {
        return data;
      });
    console.log(data);
  };

  for (let i = 0; i < dataPenawaranLocalStorage.length; i++) {
    if (dataPenawaranLocalStorage[i].id === productSelect) {
      setSudahTawar(true);
    }
  }

  const [getUser, setGetUser] = useState([]);

  useEffect(() => {
    const getPost = async () => {
      await axios
        .get("https://secondhand-api-fsw14-kel3.herokuapp.com/api/users/profile", {
          headers: {
            Authorization: `Bearer ${bearerToken}`
          }
        })
        .then((res) => {
          setGetUser(res.data);
        });
    };
    getPost();
  }, []);

  return (
    <div className="container-fluid detail-product" style={{ marginTop: "100px" }}>
      {dataProduct.data == undefined ? (
        ""
      ) : (
        <div className="row justify-content-center">
          <div className="col-lg-6">
            <div
              id="carouselExampleControlsNoTouching"
              className="carousel slide detail-product-img mx-auto"
              data-bs-ride="carousel"
              data-bs-touch="false"
              data-bs-interval="false">
              <div className="carousel-indicators">
                {dataFotoProduct != "loading" ? (
                  dataFotoProduct.map((item, i) => {
                    return (
                      <div className="indicator-carousel" key={i}>
                        <button
                          type="button"
                          data-bs-target="#carouselExampleControlsNoTouching"
                          data-bs-slide="false"
                          data-bs-slide-to={i}
                          className="active"
                          aria-current="true"
                          aria-label={"Slide" + i}></button>
                      </div>
                    );
                  })
                ) : (
                  <p>wait for a second</p>
                )}
              </div>
              <div className="carousel-inner">
                {dataFotoProduct != "loading" ? (
                  dataFotoProduct.map((item, i) => {
                    return (
                      <div className={i === 0 ? "carousel-item active" : "carousel-item"} key={i}>
                        <img
                          src={item}
                          className="d-block w-100"
                          alt="detail-product-img-1"
                          id="carouselExampleControlsNoTouching"
                        />
                      </div>
                    );
                  })
                ) : (
                  <p>wait for a second</p>
                )}
              </div>
              <button
                className="carousel-control-prev"
                type="button"
                data-bs-target="#carouselExampleControlsNoTouching"
                data-bs-slide="prev">
                <img src="icons/carousel-button.svg" alt="next btn" />
              </button>
              <button
                className="carousel-control-next"
                type="button"
                data-bs-target="#carouselExampleControlsNoTouching"
                data-bs-slide="next">
                <img src="icons/carousel-button.svg" alt="next btn" />
              </button>
            </div>
            <div className="detail-product-description mx-auto">
              <h5 className="title">Deskripsi</h5>
              {dataProduct.data.deskripsi.split("\n\n").map((item, idx) => {
                return (
                  <p key={idx} className="text ">
                    {item}
                    <br />
                  </p>
                );
              })}
            </div>
          </div>
          <div className="col-lg-3">
            <div className="detail-product-name mx-auto">
              <h5 className="title">{dataProduct.data.nama_produk}</h5>
              <p className="category">{dataProduct.data.kategori}</p>
              <h5 className="price">Rp {CurrencyFormatter(dataProduct.data.harga_produk)}</h5>
              {getUserData.id == dataProduct.data.id_seller && getUserData.exp > dateNowConvert ? (
                <>
                  <button type="button" className="btn ">
                    Terbitkan
                  </button>
                  <Link
                    to={`/updateProduk?id=${productSelect}`}
                    type="button"
                    className="btn btn-outline d-flex justify-content-center align-items-center">
                    Edit
                  </Link>
                </>
              ) : isLogin === true && getUserData.exp > dateNowConvert ? (
                <>
                  <button
                    type="button"
                    className="btn"
                    data-bs-toggle={sudahTawar === false && "modal"}
                    data-bs-target={sudahTawar === false && "#exampleModal"}
                    style={{
                      backgroundColor: sudahTawar === false ? "#7126B5" : "#D0D0D0"
                    }}>
                    Saya tertarik dan ingin nego
                  </button>
                  <button
                    type="button"
                    className="btn btn-outline"
                    onClick={() => {
                      setAlertWishlist(true);

                      if (sudahWishlist == false) {
                        let wishlist = JSON.parse(localStorage.getItem("wishlist"));
                        if (wishlist === null) {
                          wishlist = [];
                        }
                        wishlist.push({
                          id: productSelect,
                          data: dataProduct.data
                        });

                        localStorage.setItem("wishlist", JSON.stringify(wishlist));
                        setSudahWishList(true);
                        setWishlist(wishlist);
                      }
                    }}>
                    Masukan ke wishlist
                  </button>
                </>
              ) : (
                <>
                  <Link
                    to="/login"
                    className="btn d-flex justify-content-center align-items-center">
                    Saya tertarik dan ingin nego
                  </Link>
                  <button
                    type="button"
                    className="btn btn-outline"
                    onClick={() => {
                      setAlertWishlist(true);

                      if (sudahWishlist == false) {
                        let wishlist = JSON.parse(localStorage.getItem("wishlist"));
                        if (wishlist === null) {
                          wishlist = [];
                        }
                        wishlist.push({
                          id: productSelect,
                          data: dataProduct.data
                        });

                        localStorage.setItem("wishlist", JSON.stringify(wishlist));
                        setSudahWishList(true);
                        setWishlist(wishlist);
                      }
                    }}>
                    Masukan ke wishlist
                  </button>
                </>
              )}
            </div>
            <div className="detail-product-seller mx-auto d-flex justify-content-between">
              <img src={dataProduct.data.User.foto_user} alt="avatar" />
              <div className="flex-fill">
                <h5 className="title">{dataProduct.data.User.nama}</h5>
                <p className="text">
                  {dataProduct.data.User.kota == null ? "Kota" : dataProduct.data.User.kota}
                </p>
              </div>
            </div>
            <div
              className="detail-product-description-mobile mx-auto"
              style={{ marginBottom: "150px" }}>
              <h5 className="title">Deskripsi</h5>
              <p className="text">
                {dataProduct.data.deskripsi.split("\n\n").map((item, idx) => {
                  return (
                    <p key={idx} className="text ">
                      {item}
                      <br />
                    </p>
                  );
                })}
              </p>
            </div>
            {getUserData.id == dataProduct.data.id_seller ? (
              <button type="button" className="btn detail-product-buy-btn ">
                Edit
              </button>
            ) : isLogin === true && getUserData.exp > dateNowConvert ? (
              <>
                <button
                  type="button"
                  className="btn detail-product-buy-btn"
                  data-bs-toggle={sudahTawar === false && "modal"}
                  data-bs-target={sudahTawar === false && "#exampleModal"}
                  style={{
                    backgroundColor: sudahTawar === false ? "#7126B5" : "#D0D0D0",
                    bottom: "75px"
                  }}>
                  Saya tertarik dan ingin nego
                </button>
                <button
                  type="button"
                  className="btn btn-outline detail-product-buy-btn"
                  style={{
                    backgroundColor: "white",
                    border: "1px solid #7126B5",
                    color: "#7126B5"
                  }}
                  onClick={() => {
                    setAlertWishlist(true);

                    if (sudahWishlist == false) {
                      let wishlist = JSON.parse(localStorage.getItem("wishlist"));
                      if (wishlist === null) {
                        wishlist = [];
                      }
                      wishlist.push({
                        id: productSelect,
                        data: dataProduct.data
                      });

                      localStorage.setItem("wishlist", JSON.stringify(wishlist));
                      setSudahWishList(true);
                      setWishlist(wishlist);
                    }
                  }}>
                  Masukan ke wishlist
                </button>
              </>
            ) : (
              <>
                <Link
                  to="/login"
                  className="btn detail-product-buy-btn d-flex justify-content-center align-items-center">
                  Saya tertarik dan ingin nego
                </Link>
                <button
                  type="button"
                  className="btn btn-outline detail-product-buy-btn"
                  style={{
                    backgroundColor: "white",
                    border: "1px solid #7126B5",
                    color: "#7126B5"
                  }}
                  onClick={() => {
                    setAlertWishlist(true);

                    if (sudahWishlist == false) {
                      let wishlist = JSON.parse(localStorage.getItem("wishlist"));
                      if (wishlist === null) {
                        wishlist = [];
                      }
                      wishlist.push({
                        id: productSelect,
                        data: dataProduct.data
                      });

                      localStorage.setItem("wishlist", JSON.stringify(wishlist));
                      setSudahWishList(true);
                      setWishlist(wishlist);
                    }
                  }}>
                  Masukan ke wishlist
                </button>
              </>
            )}
          </div>
        </div>
      )}
      {dataProduct.data == undefined ? (
        ""
      ) : (
        <div
          className="modal fade"
          id="exampleModal"
          tabIndex="-1"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true">
          <div className="modal-dialog modal-dialog-centered">
            <div className="modal-content detail-product-modal">
              <div className="modal-header">
                <button
                  type="button"
                  className="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"></button>
              </div>
              <div className="modal-body">
                <p className="modal-text-medium">Masukkan Harga Tawarmu</p>
                <p className="modal-text-detail">
                  Harga tawaranmu akan diketahui penjual, jika penjual cocok kamu akan segera
                  dihubungi penjual.
                </p>
                <div className="d-flex modal-product">
                  <img src={dataFotoProduct[0]} alt="product" />
                  <div className="flex-fill">
                    <p className="title">{dataProduct.data.nama_produk}</p>
                    <p className="price">Rp {CurrencyFormatter(dataProduct.data.harga_produk)}</p>
                  </div>
                </div>
                <p className="modal-text-tawar">Harga Tawar</p>
                <input
                  type="number"
                  placeholder="Rp 0,00"
                  className="modal-input-tawar"
                  value={hargaTawar}
                  onChange={(e) => {
                    setHargaTawar(e.target.value);
                  }}
                />
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="modalBuyBtn"
                  onClick={() => {
                    if (hargaTawar !== "") {
                      setSudahTawar(true);
                      setAlert(true);

                      let dataPenawaranLocalStorage = JSON.parse(
                        localStorage.getItem("dataPenawaran")
                      );
                      if (dataPenawaranLocalStorage === null) {
                        dataPenawaranLocalStorage = [];
                      }
                      dataPenawaranLocalStorage.push({
                        id: dataProduct.data.id,
                        harga: hargaTawar
                      });

                      localStorage.setItem(
                        "dataPenawaran",
                        JSON.stringify(dataPenawaranLocalStorage)
                      );
                      setDataPenawaranLocalStorage(dataPenawaranLocalStorage);
                      setHargaTawar("");
                    }

                    tawarBarang();
                  }}
                  data-bs-toggle="modal"
                  data-bs-target="#exampleModal"
                  data-bs-dismiss="modal">
                  Kirim
                </button>
              </div>
            </div>
          </div>
        </div>
      )}
      {alert ? (
        <div className="row justify-content-center" style={{ top: "100px" }}>
          <div
            className="col-lg-6 alert detail-product-alert d-flex justify-content-between"
            role="alert">
            <p>Harga tawarmu berhasil dikirim ke penjual</p>
            <button
              type="button"
              data-bs-dismiss="modal"
              onClick={() => setAlert(false)}
              aria-label="Close">
              <img src="/icons/close-icon.svg" alt="close" />
            </button>
          </div>
        </div>
      ) : null}
      {alertWishlist ? (
        <div className="row justify-content-center">
          <div
            className="col-lg-6 alert detail-product-alert d-flex justify-content-between"
            style={{ top: "100px" }}
            role="alert">
            <p>Data Berhasil Masuk Wishlist</p>
            <button
              type="button"
              data-bs-dismiss="modal"
              onClick={() => setAlertWishlist(false)}
              aria-label="Close">
              <img src="/icons/close-icon.svg" alt="close" />
            </button>
          </div>
        </div>
      ) : null}
    </div>
  );
};

export default DetailProductPage;
