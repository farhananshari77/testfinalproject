import React from "react";
import NavbarHome from "../NavbarHome/NavbarHome";
import DetailPage from "./index";

export default function DetailProduct() {
  return (
    <>
      <NavbarHome />
      <DetailPage />
    </>
  );
}
