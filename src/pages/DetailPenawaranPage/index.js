/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from "react";
import "./styles.css";
import ProductCardLebar from "../../components/ProductCardLebar";
import { Link } from "react-router-dom";
import axios from "axios";
import CurrencyFormatter from "../../modules/CurrencyFormatter";
import jwt_decode from "jwt-decode";

const DetailPenawaranPage = () => {
  const bearerToken = localStorage.getItem("token");
  const isLogin = bearerToken !== null;
  const getUserData = jwt_decode(bearerToken);

  const dateNow = new Date();
  const dateNowConvert = dateNow.getTime().toString().slice(0, 10);

  if (isLogin === false || getUserData.exp < dateNowConvert) {
    window.location.href = "/login";
  }

  const [terimaPenawaran, setTerimaPenawaran] = useState(false);
  const [alert, setAlert] = useState(false);
  const [dataBarangLocalStorage, setDataBarangLocalStorage] = useState([]);
  const [dataBarangTerjualLocalStorage, setDataBarangTerjualLocalStorage] = useState([]);
  const [selesaiPenawaran, setSelesaiPenawaran] = useState(false);
  const [dataProduct, setDataProduct] = useState([]);

  const queryParams = new URLSearchParams(window.location.search);
  const productSelect = queryParams.get("id");

  useEffect(() => {
    const items = JSON.parse(localStorage.getItem("terimaPenawaran"));
    const barangTerjual = JSON.parse(localStorage.getItem("barangTerjual"));
    if (items) {
      setDataBarangLocalStorage(items);
    }

    if (barangTerjual) {
      setDataBarangTerjualLocalStorage(barangTerjual);
    }
  }, []);

  const url = `https://secondhand-api-fsw14-kel3.herokuapp.com/api/tawaran/${productSelect}`;

  const getPost = async () => {
    await axios
      .get(url, {
        headers: {
          Authorization: `Bearer ${bearerToken}`
        }
      })
      .then((res) => {
        setDataProduct(res.data);
      });
  };
  console.log("data: ", dataProduct);

  useEffect(() => {
    getPost();
  }, []);
  console.log(dataProduct);
  const urlSelesaikanPenawaran = dataProduct.data
    ? `https://secondhand-api-fsw14-kel3.herokuapp.com/api/transaksi/${dataProduct.data.Barang.id}/${productSelect}`
    : "";
  const urlTerimaPenawaran = dataProduct.data
    ? `https://secondhand-api-fsw14-kel3.herokuapp.com/api/tawaran/terima/${productSelect}/${dataProduct.data.Barang.id}`
    : "";

  const postSelesaikanPenawaran = async () => {
    const data = {
      nominal_bayar: parseInt(dataProduct.data.harga_tawaran)
    };
    await fetch(urlSelesaikanPenawaran, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${bearerToken}`
      },
      body: JSON.stringify(data)
    })
      .then((res) => res.json())
      .then((data) => {
        return data;
      });
    console.log("databayar: ", data);
  };
  const postTawaranDiterima = async () => {
    await fetch(urlTerimaPenawaran, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${bearerToken}`
      }
    })
      .then((res) => res.json())
      .then((data) => {
        return data;
      });
  };

  for (let i = 0; i < dataBarangLocalStorage.length; i++) {
    if (dataBarangLocalStorage[i].id === productSelect) {
      setTerimaPenawaran(true);
    }
  }
  if (selesaiPenawaran == true) {
    postTawaranDiterima();
  }
  for (let i = 0; i < dataBarangTerjualLocalStorage.length; i++) {
    if (dataBarangTerjualLocalStorage[i].id === productSelect) {
      setSelesaiPenawaran(true);
    }
  }

  const dataFotoUser = dataProduct.data ? dataProduct.data.User.foto_user : "loading";
  const dataFotoProduct = dataProduct.data
    ? JSON.parse(dataProduct.data.Barang.foto_produk)
    : "loading";

  return (
    <div className="container-fluid detail-penawaran-page">
      {dataProduct.data == undefined ? (
        ""
      ) : (
        <div className="row justify-content-center ">
          <div className="col-lg-9">
            <h3 className="title-main">Detail Data Penawaran</h3>
            <h3 className="lead">Penawar Barangmu :</h3>
            <div className="detail-penawaran-page-seller mx-auto d-flex justify-content-between align-items-center">
              <img src={dataFotoUser == null ? "images/avatar.png" : dataFotoUser} alt="avatar" />
              <div className="flex-fill">
                <h5 className="title">{dataProduct.data.User.nama}</h5>
                <p className="text">
                  {dataProduct.data.User.kota == null ? "Kota" : dataProduct.data.User.kota}
                </p>
              </div>
            </div>
            <h3 className="lead">Barangmu Yang Ditawar :</h3>
            <ProductCardLebar
              harga={dataProduct.data.Barang.harga_produk}
              nama={dataProduct.data.Barang.nama_produk}
              hargaPenawaran={
                dataProduct.data.harga_tawaran == null ? "-" : dataProduct.data.harga_tawaran
              }
              tanggalPenawaran={dataProduct.data.updatedAt}
              type="Penawaran"
              href=""
              image={dataFotoProduct == null ? "images/product-1.png" : dataFotoProduct[0]}
            />
            {terimaPenawaran === false && (
              <div className="d-flex gap-3 mt-5 justify-content-center">
                <Link className="btn shadow-none" to="/daftarJual">
                  Tolak
                </Link>
                <button
                  type="button"
                  className="btn shadow-none"
                  data-bs-toggle={terimaPenawaran === false && "modal"}
                  data-bs-target={terimaPenawaran === false && "#exampleModal"}
                  onClick={() => {
                    terimaPenawaran === true && setAlert(true);
                  }}
                  style={{
                    backgroundColor: terimaPenawaran === false ? "#7126B5" : "#D0D0D0"
                  }}>
                  Terima
                </button>
              </div>
            )}
            <div
              className="modal fade"
              id="exampleModal"
              tabIndex="-1"
              aria-labelledby="exampleModalLabel"
              aria-hidden="true">
              <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content detail-product-modal">
                  <div className="modal-header">
                    <button
                      type="button"
                      className="btn-close"
                      data-bs-dismiss="modal"
                      aria-label="Close"></button>
                  </div>
                  <div className="modal-body">
                    <p className="modal-text-medium lead">
                      Yeay kamu berhasil mendapat harga yang sesuai
                    </p>
                    <p className="modal-text-detail">
                      Segera hubungi pembeli melalui whatsapp untuk transaksi selanjutnya
                    </p>
                    <div className="modal-product">
                      <p className="modal-text-medium text-center">Product Match</p>
                      <div className="d-flex">
                        <img src="images/avatar.png" alt="avatar" />
                        <div className="flex-fill">
                          <p className="title">{dataProduct.data.User.nama}</p>
                          <p className="price">
                            {dataProduct.data.User.kota == null
                              ? "Kota"
                              : dataProduct.data.User.kota}
                          </p>
                        </div>
                      </div>
                      <div className="d-flex mt-4">
                        <img
                          src={
                            dataFotoProduct == null ? "images/product-1.png" : dataFotoProduct[0]
                          }
                          alt="product"
                        />
                        <div className="flex-fill">
                          <p className="title">{dataProduct.data.Barang.nama_produk}</p>
                          <p className="price">
                            Rp{" "}
                            {CurrencyFormatter(
                              dataProduct.data.harga_tawaran == null
                                ? "-"
                                : dataProduct.data.harga_tawaran
                            )}
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="modal-footer">
                    <a
                      href={`https://wa.me/${dataProduct.data.User.no_hp}?text=Hai%20${
                        dataProduct.data.User.nama
                      },%20saya%20mau%20memnerima%20tawaranmu%20dengan%20harga%20${CurrencyFormatter(
                        dataProduct.data.harga_tawar == null ? "-" : dataProduct.data.harga_tawar
                      )}`}
                      target="_blank"
                      rel="noopener noreferrer"
                      className="modalBuyBtn d-flex justify-content-around align-items-center"
                      onClick={() => {
                        setTerimaPenawaran(true);

                        let terimaPenawaran = JSON.parse(localStorage.getItem("terimaPenawaran"));
                        if (terimaPenawaran === null) {
                          terimaPenawaran = [];
                        }
                        terimaPenawaran.push({
                          id: dataProduct.data.id
                        });

                        localStorage.setItem("terimaPenawaran", JSON.stringify(terimaPenawaran));
                      }}>
                      Hubungi via Whatsapp
                      <img src="./icons/fi_whatsapp.svg" alt="wa-icon" />
                    </a>
                  </div>
                </div>
              </div>
            </div>
            {alert ? (
              <div className="row justify-content-center">
                <div
                  className="col-lg-6 alert detail-product-alert d-flex justify-content-between"
                  role="alert">
                  <p>Anda telah menerima tawaran pembelian ini !</p>
                  <button
                    type="button"
                    data-bs-dismiss="modal"
                    onClick={(e) => setAlert(false)}
                    aria-label="Close">
                    <img src="/icons/close-icon.svg" alt="close" />
                  </button>
                </div>
              </div>
            ) : null}
            {terimaPenawaran === true && (
              <div className="d-flex gap-3 mt-5 justify-content-center">
                <Link className="btn shadow-none" to="/daftarJual">
                  Kembali Ke Daftar Jual
                </Link>
                <button
                  type="button"
                  className="btn shadow-none"
                  data-bs-toggle={selesaiPenawaran === false && "modal"}
                  data-bs-target={selesaiPenawaran === false && "#modalSelesaiTransaksi"}
                  style={{
                    backgroundColor: selesaiPenawaran === false ? "#7126B5" : "#D0D0D0",
                    color: "white",
                    border: "none"
                  }}>
                  Selesaikan Transaksi
                </button>
              </div>
            )}
            <div
              className="modal fade"
              id="modalSelesaiTransaksi"
              tabIndex="-1"
              aria-labelledby="modalSelesaiTransaksiLabel"
              aria-hidden="true">
              <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content detail-product-modal">
                  <div className="modal-header">
                    <button
                      type="button"
                      className="btn-close"
                      data-bs-dismiss="modal"
                      aria-label="Close"></button>
                  </div>
                  <div className="modal-body">
                    <p className="modal-text-medium lead">Selesaikan Penawaran ?</p>
                    <p className="modal-text-detail">
                      Pastikan transaksimu telah sesuai sebelum menyelesaikan transaksi
                    </p>
                  </div>
                  <div className="modal-footer">
                    <div
                      className="modalBuyBtn d-flex justify-content-around align-items-center"
                      onClick={() => {
                        let selesaiPenawaran = JSON.parse(localStorage.getItem("barangTerjual"));
                        if (selesaiPenawaran === null) {
                          selesaiPenawaran = [];
                        }
                        selesaiPenawaran.push({
                          id: dataProduct.data.id
                        });

                        localStorage.setItem("barangTerjual", JSON.stringify(selesaiPenawaran));
                        setSelesaiPenawaran(true);

                        postSelesaikanPenawaran();
                      }}
                      data-bs-toggle="modal"
                      data-bs-target="#modalSelesaiTransaksi"
                      data-bs-dismiss="modal">
                      Selesaikan Penawaran
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default DetailPenawaranPage;
