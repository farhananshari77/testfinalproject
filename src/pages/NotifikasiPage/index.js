import React, { useEffect, useState } from "react";
import "./styles.css";
import ProductCardLebar from "../../components/ProductCardLebar";
import axios from "axios";
import NavbarHome from "../NavbarHome/NavbarHome";
import jwt_decode from "jwt-decode";
import EmptyList from "../../components/EmptyList";
import DateNow from "../../modules/DateNow";

const NotifikasiPage = () => {
  const bearerToken = localStorage.getItem("token");
  const isLogin = bearerToken !== null;
  const getUserData = jwt_decode(bearerToken);

  if (isLogin === false || getUserData.exp < DateNow()) {
    window.location.href = "/login";
  }

  const urlNotifikasi = "https://secondhand-api-fsw14-kel3.herokuapp.com/api/tawaran/barangku";

  const [notifikasi, setNotifikasi] = useState([]);

  const getNotifikasi = async () => {
    await axios
      .get(urlNotifikasi, {
        headers: {
          Authorization: `Bearer ${bearerToken}`
        }
      })
      .then((res) => {
        setNotifikasi(res.data);
      });
  };

  useEffect(() => {
    getNotifikasi();
  }, []);
  const dataNotifikasi = notifikasi && notifikasi.data ? notifikasi.data : [];

  return (
    <div className="container-fluid">
      <NavbarHome />
      <div className="row justify-content-center notifikasi-page" style={{ marginTop: "100px" }}>
        <div className="col-lg-9">
          <h3 className="title">Daftar Notifikasimu</h3>
          <div>
            {dataNotifikasi.length === 0 ? (
              <EmptyList type={"transaksi"} />
            ) : (
              dataNotifikasi.map((item) => {
                const image = JSON.parse(item.Barang.foto_produk);
                return (
                  <ProductCardLebar
                    key={item.id}
                    harga={item.Barang.harga_produk}
                    nama={item.User.nama}
                    hargaPenawaran={item.harga_tawaran == null ? 0 : item.harga_tawaran}
                    tanggalPenawaran={item.updateAt}
                    type="Penawaran"
                    href={`/detailPenawaran?id=${item.id}`}
                    image={image[0]}
                  />
                );
              })
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default NotifikasiPage;
