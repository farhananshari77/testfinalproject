import React from "react";
import NavbarHome from "../NavbarHome/NavbarHome";
import DetailSellerPage from "./index";

export default function DetailProductSeller() {
  return (
    <>
      <NavbarHome />
      <DetailSellerPage />
    </>
  );
}
