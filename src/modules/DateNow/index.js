const DateNow = () => {
  const date = new Date();
  return date.getTime().toString().slice(0, 10);
};

export default DateNow;
